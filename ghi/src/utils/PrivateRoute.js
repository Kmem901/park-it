import { Navigate, Outlet } from 'react-router-dom'

const PrivateRoutes = () => {
    const userIsLoggedIn = localStorage.getItem("userId")
    return (
        userIsLoggedIn ? <Outlet /> : <Navigate to='/' />
    )
}
export default PrivateRoutes
