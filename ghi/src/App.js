import { BrowserRouter, Routes, Route } from "react-router-dom";
import NeedASpotPage from "./Pages/NeedASpotPage";
import './styles/App.css';
import HomePage from "./Pages/HomePage";
import Nav from "./Nav";
import "./styles/HomePage.css";
import ParkingSpotForm from "./Pages/ParkingSpotForm.js";
import AccountPage from "./Pages/AccountPage.js";
import ParkingSpotEditForm from "./Pages/ParkingSpotEditForm.js";
import { AuthProvider, useToken } from "./Authentication";
import LoginComponent from "./Pages/LoginComponent.js";
import SignupComponent from "./Pages/SignupComponent.js";
import PrivateRoutes from './utils/PrivateRoute'


function GetToken() {
  useToken();
  return null
}

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');
  return (
    <>
      <BrowserRouter basename={basename}>
        <Nav />
        <AuthProvider>
          <GetToken />
          <div>
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/login" element={<LoginComponent />} />
              <Route path="/signup" element={<SignupComponent />} />
              <Route element={<PrivateRoutes />}>
                <Route path="/parking-spot-form" element={<ParkingSpotForm />} />
                <Route path="/account" element={<AccountPage />} />
                <Route path='/parking-spot/:id' element={<ParkingSpotEditForm />} />
                <Route path="/parking-spots" element={<NeedASpotPage />} />
              </Route>
            </Routes>
          </div>
        </AuthProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
