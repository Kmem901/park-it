import React, { useEffect, useState } from "react";
import "../styles/ParkingSpot.css";
import { useAuthContext } from "../Authentication"

function Inputs(props) {
  const { value, onChange, type, id, placeholder } = props;

  return (
    <div className="form-input-wrapper flexbox-left">
      <input
        value={value}
        onChange={onChange}
        required
        type={type}
        id={id}
        placeholder={placeholder}
        className="form-input"
      />
    </div>
  )
}

function ParkingSpotForm(props) {
  const [street, setStreet] = useState('');
  const [cities, setCity] = useState('');
  const [states, setStates] = useState([]);
  const [zipcode, setZipcode] = useState('');
  const [state, setStateAbbr] = useState('');
  const { token } = useAuthContext();

  useEffect(() => {
    async function getStates() {
      const urlStates = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/states`;
      const response = await fetch(urlStates);
      if (response.ok) {
        const data = await response.json();
        setStates(data);
      }
    }
    getStates();
  }, [])

  function handleInput() {
    document.getElementById("alert").classList.add("hidden")
  }


  // logic to separate and add special characters for map box query
  const elements = (street.split(" "));
  const number = elements.shift();
  const street_abbr = elements.pop()
  const street_name_with_spaces = elements.join(" ")
  const street_name = street_name_with_spaces.replaceAll(" ", "%20")
  const city = cities.replaceAll(" ", "%20")

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      number: number,
      street_name: street_name,
      street_abbr: street_abbr,
      city: city,
      state: state,
      zipcode: zipcode,
      date: new Date(),
      available: true,
      owner_id: localStorage.getItem("userId"),
    };

    const urlNewSpot = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/parking_spots`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    };

    const response = await fetch(urlNewSpot, fetchConfig);
    if (response.ok) {
      const alert = document.getElementById("alert");
      alert.classList.remove("hidden")

      setStreet('');
      setCity('');
      setStates([]);
      setZipcode('');
    }
  }


  return (
    <>
      <section className="hero_parking">
        <div className="container">
          <form onChange={handleInput} onSubmit={handleSubmit} className="form">
            <h2 className="form__title">Parking Spot Form</h2>
            <div className="form-input-max">
              <Inputs
                value={street}
                onChange={e => setStreet(e.target.value)}
                type="text"
                id="street"
                placeholder="Street"
              />
            </div>
            <div className="form-input-grid">
              <Inputs
                value={cities}
                onChange={e => setCity(e.target.value)}
                type="text"
                id="city"
                placeholder="City"
              />
              <div className="flexbox-left">
                <select className="form-input" onChange={e => setStateAbbr(e.target.value)}>
                  <option>State</option>
                  {states.map(state => <option key={state.abbreviation} value={state.abbreviation}>{state.name}</option>)}
                </select>
              </div>
              <Inputs
                value={zipcode}
                onChange={e => setZipcode(e.target.value)}
                type="text"
                id="zipcode"
                placeholder="Zipcode"
              />
            </div>
            <button className="btn">Save</button>
            <div id="alert" className="alert hidden">Thank you for adding a parking spot!</div>
          </form>
        </div>
      </section>
    </>
  );
};

export default ParkingSpotForm;
