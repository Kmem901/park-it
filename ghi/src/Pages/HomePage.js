import nameplate from "./../assets/images/parkitnameplate.png";
import { Link } from 'react-router-dom';


function HomePage() {
  const userIsLoggedIn = localStorage.getItem("userId")

  return (
    <div>
      <section >
        <header className="hero-header">
          <h1>
            <img src={nameplate} className="hero-title" alt=" " />
          </h1>
        </header>
        <div className="hero-footer">
          <Link className={`btn_mini_sign_in button ${userIsLoggedIn ? "hidden" : ""}`} to="/login">
            Log In to find a spot!
          </Link>
          <Link className={`btn_mini_sign_in button ${userIsLoggedIn ? "" : "hidden"}`} to="/parking-spot-form">
            Found a Spot?
          </Link>
          <Link className={`btn_mini_sign_in button ${userIsLoggedIn ? "" : "hidden"}`} to="/parking-spots">
            Need a Spot?
          </Link>
        </div>
      </section>
    </div>
  );
}
export default HomePage;
