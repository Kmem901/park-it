## ParkIt API Design

## Sign in/Log out

### Sign in

- Endpoint path: `/token`
- Endpoint Method: "POST"
- Request shape:

  ```json
  "username": "string",
  "password": "string",
  ```

- Response: User information and token
- Response shape (JSON):
  ```json
  {
    "access_token": "string",
    "token_type": "Bearer",
    "account": {
      "id": "int",
      "username": "string"
    }
  }
  ```

### Log out

- Endpoint path: `/token`
- Endpoint method: DELETE

- Headers:

  - Authoriation: Bearer token

- Response: Always true
- Response shape (JSON):

````json
    true
    ```


## Account

### Sign up

- Endpoint path: ```/api/accounts```
- Endpoint path: "POST"

- Request shape:
    ```json
    "username": "string",
    "password": "password"
    ```

- Response: User information and token
- Response shape (JSON):
    ``` json
    {
        "access_token": "string",
        "token_type": "Bearer",
        "account": {
            "id": "int",
            "username": "email"
        }
    }
    ```

## Profile

### Account Page (*After user has been logged in*)
- Endpoint path: ```/api/accounts/{account_id}
- Endpoint method: "GET"

- Headers:
    -Authorization: Bearer token

- Response: Account Details
- Response shape (JSON):
    ``` json
    {
        "id": int,
        "name": "string",
        "username": "string"
    }
    ```
## Parking Spots

### Parking Spot list
- Endpoint path: ```/api/parking_spots```
- Endpoint method: "GET"

-Headers:
    - Authorization: Bearer token

- Response: Parking Spot List
- Response shape (JSON):
    ``` json
    {
        "id": int,
        "number": int,
        "street_name": "string",
        "street_abbr": "string",
        "city": "string",
        "state": "string",
        "zipcode": "int",
        "date": "int",
        "lat": "int",
        "lon": "int",
        "available": "boolean"
        "owner_id": "int",
    }
    ```

### Creating a Parking Spot (*create*)
- Endpoint path: ```/api/parking_spots/```
- Endpoint method: "POST"

- Headers:
    -Authorization: Bearer token

- Response: Creating a parking spot
- Response shape (JSON)
    ``` json
    {
        "number": int,
        "street_name": "string",
        "street_abbr": "string",
        "city": "string",
        "state": "string",
        "zipcode": "int",
        "date": "int",
        "available": "boolean",
        "owner_id": "int",
    }
    ```

### Parking Spot Detail

- Endpoint path: ```/api/parking_spots/{parking_spot_id}/```
- Endpoint method: "GET"

- Headers:
    - Authorization: Bearer token

- Response: Parking spot Detail
- Response shape (JSON)
    ``` json
    {
        "id": int,
        "number": int,
        "streen_name": "string",
        "street_abbr": "string",
        "city": "string",
        "state": "string",
        "zipcode": "int",
        "date": "int",
        "lat": "int",
        "lon": "int",
        "available": "boolean",
        "owner_id": "int",
    }

### Edit Parking Spot (*Edit form*)

- Endpoint path: ```/api/parking_spots/{parking_spot_id}
- Endpoint method: "PUT"

- Headers:
    - Authorization: Bearer token

- Response: Edit Parking Spot
- Response shape (JSON)
    ``` json
    {
        "number": "int",
        "street_name": "string",
        "street_abbr": "string",
        "city": "string",
        "state": "string",
        "zipcode": "int",
        "date": "int",
        "available": "boolean",
        "owner_id": "int",
    }

### Delete Parking Spot (*delete action*)

- Endpoint path: ```/api/parking_spots/{parking_spot_id}
- Endpoint method: "DELETE"

- Headers:
    - Authorization: Bearer token

- Response: Delete existing parking spot
- Response shape (JSON)
    ``` json
    {
        "boolean"
    }
````
